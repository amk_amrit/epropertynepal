import 'package:flutter/material.dart';
import 'package:sandip_app/screens/homepage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Propery Management',
      theme: ThemeData(
        brightness:     Brightness.light,
        primaryColor:   Colors.lightGreen[800],
        accentColor:    Colors.cyan[600],
      ),
      home: HomePage(),
    );
  }
}