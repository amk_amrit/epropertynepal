import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us'),
      ),
      body: Column(
        children: <Widget>[
          Card(
            child: Text("To make our real estate servies as cost effective as possible. To explore and innovate ideas in order to kame the selling and buying of property faster, hassle-free and easier. To be the leading real estate service company form mechi to mahakali, while always exceeding our customer's expections",
            style: TextStyle(
              fontSize: 17.0,
            ),),
          ),
          RaisedButton(
            color: Colors.tealAccent,
            onPressed: (){
              Navigator.pop(context);
            },
            child: Text("Go Back",
             style: TextStyle(
               color: Colors.black,
             ),),

          )
        ],
      ),
    );
  }
}