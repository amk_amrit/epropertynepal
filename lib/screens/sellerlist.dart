import 'package:flutter/material.dart';
import 'package:sandip_app/screens/crud/edit.dart';
import 'package:sandip_app/screens/crud/view.dart';

class SellerList extends StatefulWidget {
  @override
  _SellerListState createState() => _SellerListState();
}

class _SellerListState extends State<SellerList> {
  Widget bodyData() => DataTable(
    onSelectAll: (b) {},
    sortColumnIndex: 0,
    sortAscending: true,
        columns: <DataColumn>[
          // DataColumn(
          //   label: Text("Owner Name"),
          //   numeric: false,
          //   onSort: (i, b) {
          //     setState(() {
          //         properyLists.sort((a,b) => a.ownerName.compareTo(b.ownerName));
          //     });
          //   },
          //   tooltip: "To display owner Name",
          // ),
          DataColumn(
            label: Text("Address"),
            numeric: false,
            onSort: (i, b) {
              setState(() {
                  properyLists.sort((a,b) => a.address.compareTo(b.address));
              });
            },
            tooltip: "To display Address",
          ),
          // DataColumn(
          //   label: Text("Phone Number "),
          //   numeric: false,
          //   onSort: (i, b) {
          //     setState(() {
          //         properyLists.sort((a,b) => a.phoneNumber.compareTo(b.phoneNumber));
          //     });
          //   },
          //   tooltip: "To display Phone number ",
          // ),
          DataColumn(
            label: Text("Action"),
            numeric: false,
            onSort: (i, b) {},
            tooltip: "To display Action ",
          )
        ],
        rows: properyLists
        .map((properyList)=>DataRow(cells: [
          // DataCell(
          //   Text(properyList.ownerName),
          //   showEditIcon: false,
          //   placeholder: false,
          // ),
          DataCell(
            Text(properyList.address),
            showEditIcon: false,
            placeholder: false,
          ),
          // DataCell(
          //   Text(properyList.phoneNumber),
          //   showEditIcon: false,
          //   placeholder: false,
          // ),
          DataCell(
                     Card(
                elevation: 2.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.info),
                          color: Colors.blueAccent,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              return ViewProperty();
                            }));
                          }),
                      IconButton(
                          icon: Icon(Icons.edit),
                          color: Colors.red,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              return EditProperty();
                            }));
                          }),
                      IconButton(
                          icon: Icon(Icons.delete),
                          color: Colors.green,
                          onPressed: () {
                          }),
                    ],
                  ),
                ),
              ),
          ),
        ]))
      .toList());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Propery Seller Lists"),
      ),
      body: Container(
        child: bodyData(),
      ),
    );
  }
}

class properyList {
  String ownerName;
  String address;
  String phoneNumber;
  properyList({this.ownerName, this.address, this.phoneNumber});
}

var properyLists = <properyList>[
  properyList(
      ownerName: "Amrit Kafle",
      address: "New Baneshower",
      phoneNumber: "9849437903"),
  properyList(
      ownerName: "Sandip Adhikari",
      address: "Naya Thimi",
      phoneNumber: "9849437905"),
  properyList(
      ownerName: "Sager Nepal",
      address: "Kausaltar",
      phoneNumber: "9849437909"),
  properyList(
      ownerName: "Santosh Adhikari",
      address: "New Road",
      phoneNumber: "9849437910"),
];
