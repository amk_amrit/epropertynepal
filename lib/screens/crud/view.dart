import 'package:flutter/material.dart';
import 'package:sandip_app/screens/crud/edit.dart';

class ViewProperty extends StatefulWidget {
  @override
  _ViewPropertyState createState() => _ViewPropertyState();
}

class _ViewPropertyState extends State<ViewProperty> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dtails View"),
      ),
      body: Card(
              child: Column(
          children: <Widget>[
            Center(
              child: Row(
                  children: <Widget>[
                    Expanded(
                      child:Padding(
                        padding: const EdgeInsets.only(
                          left: 20.0,
                        ),
                        child: Text("Owner Name"),
                      )
                    ),
                    Expanded(
                      child: Text("Amrit Kafle"),
                    ),
                  ],
                ),
            ),
            SizedBox(
              height: 14.0,
            ),
              Center(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child:Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text("Address"),
                      )
                    ),
                    Expanded(
                      child: Text("New Baneshower"),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 14.0,
              ),
              Center(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child:Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text("Phone Number"),
                      )
                    ),
                    Expanded(
                      child:Text("9849437904")
                    ),
                  ],
                ),
              ),

             Card(
                  elevation: 2.0,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        IconButton(
                            icon: Icon(Icons.edit),
                            color: Colors.red,
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) {
                                return EditProperty();
                              }));
                            }),
                        IconButton(
                            icon: Icon(Icons.delete),
                            color: Colors.green,
                            onPressed: () {
                            }),
                      ],
                    ),
                  ),
                ),
          ],
        ),
      ),
    );
  }
}