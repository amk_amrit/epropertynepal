import 'package:flutter/material.dart';

class PropertyCreate extends StatefulWidget {
  @override
  _PropertyCreateState createState() => _PropertyCreateState();
}

class _PropertyCreateState extends State<PropertyCreate> {
  var _forType = ['For Sell', 'For Buy', 'For Rent'];
  var _values = "For Sell";
  var _displayText = "";
    var _forPriority = ['Serious','High', 'Medium', 'Low'];
  var _value = "Serious";
  var _displayTextP = "";
  TextEditingController textBox1 = TextEditingController();
  TextEditingController textBox2 = TextEditingController();
  TextEditingController textBox3 = TextEditingController();
  TextEditingController textBox4 = TextEditingController();
  TextEditingController textBox5 = TextEditingController();
  TextEditingController textBox6 = TextEditingController();
  TextEditingController textBox7 = TextEditingController();
  TextEditingController textBox8 = TextEditingController();
  TextEditingController textBox9 = TextEditingController();
  TextEditingController textBox10 = TextEditingController();
  TextEditingController textBox11 = TextEditingController();
  TextEditingController textBox12 = TextEditingController();
  TextEditingController textBox13 = TextEditingController();
  TextEditingController textBox14 = TextEditingController();
  TextEditingController textBox15 = TextEditingController();
  TextEditingController textBox16 = TextEditingController();
  TextEditingController textBox17 = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text("Create Seller Property"),
      ),
      body: ListView(
          padding: EdgeInsets.all(5),
          children: <Widget>[
            DropdownButton(
              value: _values,
              isExpanded: true,
              items: _forType.map((String values) {
                return DropdownMenuItem<String>(
                  value: values,
                  child: Text(values),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  _values = value;
                  print("Value changed to $value");
                });
              },
            ),
            TextField(
              controller: textBox1,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: "Owner Name",
                  hintText: "E propery Nepal",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox2,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Price",
                  hintText: "99999",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox3,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Area",
                  hintText: "1-0-1-1",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox4,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: "facing Direction",
                  hintText: "North",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox5,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: "Road Access",
                  hintText: "10 M from Main Road",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox5,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: "Parking",
                  hintText: "10 Cars",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox6,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Buliding Age",
                  hintText: "10 Years",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox6,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Storey",
                  hintText: "3",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
          
           Row(
              children: <Widget>[
                Expanded(
                  child:TextField(
              controller: textBox7,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: "District",
                  hintText: "Kathmandu",
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0))),
            ),
                ),
              Expanded(
                  child:TextField(
              controller: textBox8,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: "Nagarpalika/GauPalika",
                  hintText: "Kathmandu Nagarpalika",
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0))),
            ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child:TextField(
              controller: textBox9,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: "City",
                  hintText: "New Banehower",
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0))),
            ),
                ),
                Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
              Expanded(
                  child:TextField(
              controller: textBox10,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Wada No",
                  hintText: "16",
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0))),
            ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child:TextField(
              controller: textBox11,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Bed Room",
                  hintText: "8",
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0))),
            ),
                ),
                Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
              Expanded(
                  child:TextField(
              controller: textBox12,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Bath Room ",
                  hintText: "3",
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0))),
            ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child:TextField(
              controller: textBox13,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "living Room",
                  hintText: "6",
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0))),
            ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox14,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Number",
                  hintText: "9849437904",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            TextField(
              controller: textBox15,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Total Room",
                  hintText: "7",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            DropdownButton(
              value: _value,
              isExpanded: true,
              items: _forPriority.map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  _value = value;
                  print("Value changed to $value");
                });
              },
            ),
            ],
          ),
          );
  }
}