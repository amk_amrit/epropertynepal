import 'package:flutter/material.dart';
import 'package:sandip_app/screens/buyerlist.dart';
import 'package:sandip_app/screens/propertycreate.dart';
import 'package:sandip_app/screens/sellerlist.dart';
import './aboutpage.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              padding: EdgeInsets.all(0),
              child: Container(
                color: Colors.lightGreen[800],
                child: Center(
                  child: Text(
                    "Epropery Nepal ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30.0,
                    ),
                  ),
                ),
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.pop(context);
              },
              leading: Icon(Icons.home),
              title: Text("Home"),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return PropertyCreate();
                  }),
                );
              },
              leading: Icon(Icons.create),
              title: Text("Create Property"),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return BuyerList();
                }));
              },
              leading: Icon(Icons.featured_play_list),
              title: Text("Buyer List"),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return SellerList();
                }));
              },
              leading: Icon(Icons.featured_play_list),
              title: Text("Seller List"),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return AboutPage();
                  }),
                );
              },
              leading: Icon(Icons.info_outline),
              title: Text("About"),
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Text("Epropery Nepal"),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text("Seller List"),
            subtitle: Text("Enter a seller propery"),
          )
        ],
      ),
    );
  }
}
